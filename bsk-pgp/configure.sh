#!/bin/bash
#
# Piotr Styczynski (ps386038)
# BSK
#
#

C_RED=$(printf "\e[1;31m")
C_GREEN=$(printf "\e[1;32m")
C_BLUE=$(printf "\e[1;34m")
C_CYAN=$(printf "\e[1;36m")
C_PURPLE=$(printf "\e[1;35m")
C_YELLOW=$(printf "\e[1;33m")
C_GRAY=$(printf "\e[1;30m")
C_NORMAL=$(printf "\e[0m")
C_BOLD=$(printf "\e[1m")

B_DEBUG=$C_GRAY
E_DEBUG=$C_NORMAL
B_ERR=$C_RED
E_ERR=$C_NORMAL
B_INFO=$C_BLUE
E_INFO=$C_NORMAL
B_WARN=$C_YELLOW
E_WARN=$C_NORMAL
B_BOLD=$C_BOLD
E_BOLD=$C_NORMAL
B_OK=$C_GREEN
E_OK=$C_NORMAL

install_done_aptupdate="false"

function configuressh {
  ssh-keygen -f "$1.key"
  if [ ! -d "/home/$1/.ssh" ]; then
    mkdir "/home/$1/.ssh"
  fi
  cp "$1.key" "/home/$1/.ssh/id_rsa"
  chown -R $1:$1 /home/$1/.ssh
  ssh-copy-id -i "$1.key" ps386038@students.mimuw.edu.pl

  printf "\n# Auto append by ./configure.sh\n`eval ssh-agent`\n" >> "/home/$1/.bashrc"
  printf "ssh-add\necho Hello!\n" >> "/home/$1/.bashrc"

  rm "$1.key"
  rm "$1.key.pub"
}

function configuresshserver {
  cp ./sshd_config /etc/ssh/sshd_config
  cp ./hosts.deny /etc/hosts.deny
}

function configuresshfs {
  mkdir /ZdalneObrazy
  sshfs ps386038@students.mimuw.edu.pl:/home/students/inf/PUBLIC/BSK /ZdalneObrazy
  mkdir /ZdalneBSK

  # Normal rsync
  rsync -chavzP --stats ps386038@students.mimuw.edu.pl:/home/students/inf/PUBLIC/BSK /ZdalneBSK

  # Turn off compression
  # rsync -chavzP -e "ssh -T -c arcfour -o Compression=no -x" --stats ps386038@students.mimuw.edu.pl:/home/students/inf/PUBLIC/BSK /ZdalneBSK

  # Exclude files
  # rsync -chavzP -e "ssh -T -c arcfour -o Compression=no -x" --exclude "dir/*.txt" --stats ps386038@students.mimuw.edu.pl:/home/students/inf/PUBLIC/BSK /ZdalneBSK

}

function configurepgp {
  gpg --gen-key
}

function aptupdate {
  if [ "$install_done_aptupdate" == "false" ]; then
    install_done_aptupdate="true"
    apt-get update -y
  fi
}

function removeuser {
  rm -r -f -d "/home/$1" > /dev/null 2> /dev/null
  userdel "$1"
}

function createuser {
  cat /etc/passwd | grep ${1} >/dev/null 2>&1
  if [ $? -eq 0 ] ; then
    printf "${B_INFO}[Info] User $1 exists. Skipping.${E_INFO}\n"
  else
    printf "${B_INFO}[Info] Create user account: $1 ${E_INFO}\n"
    useradd "$1"
  fi

  usermod --password $(echo $2 | openssl passwd -1 -stdin) $1

  if [ -d "/home/$1" ]; then
    printf "${B_INFO}[Info] Home directory /home/$1 exists. Skipping.${E_INFO}\n"
  else
    printf "${B_INFO}[Info] Creating home directory at /home/$1.${E_INFO}\n"
    mkhomedir_helper "$1"
  fi
}

function install {
  dpkg -l | grep "$1" > /dev/null 2> /dev/null
  ret="$?"
  if [ "$ret" == "0" ]; then
    printf "${B_INFO}[Info] Package $1 is already installed. Skipping.${E_INFO}\n"
  else
    printf "${B_INFO}[Info] Installing package $1...${E_INFO}\n"
    aptupdate
    apt-get install -y "$1"
  fi
}

function configup {

  install openssh-server
  install rsync
  install sshfs

  createuser u1 "pass1"
  createuser u2 "pass2"
  createuser u3 "pass3"

  configuressh u1
  configuressh u2
  configuressh u3

  configuresshserver
}

function configdown {
  removeuser u1
  removeuser u2
  removeuser u3
}


case "$1" in
        up)
            configup
            ;;
        down)
            configdown
            ;;
        task2)
            configuresshserver
            ;;
        task3)
            configuresshfs
            ;;
        *)
            echo $"Usage: $0 {up|down}"
            exit 1
esac
